package module;

import java.util.regex.Pattern;

public class Specialiste extends Medecin{
	
	private String specialite;
	
	public Specialiste(String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String email, int numAgreement, String specialite) throws MissMatchException {
		super(nom,prenom,adresse,codePostal,ville,telephone,email,numAgreement);
		this.specialite = specialite;
		
	}









public String getSpecialite() {
	return specialite;
}

public void setSpecialite(String specialite) throws MissMatchException{
	if (specialite==null||specialite.trim().isEmpty()) {
		throw new MissMatchException("Saisie de la spécialité incorrecte");}
	if(!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", specialite)) {
		throw new MissMatchException("Veuillez entrer une spécialité valide ");
	}
	else {
	this.specialite = specialite;}
	}
}



