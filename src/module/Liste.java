package module;

import java.util.ArrayList;

public class Liste {
	
	private ArrayList <Client> listeClient = new ArrayList<Client>();
	private ArrayList <Medecin> listeMedecin = new ArrayList<Medecin>();
	private ArrayList <Specialiste> listeSpecialiste = new ArrayList<Specialiste>();
	private ArrayList <Medicament> listeMedicament = new ArrayList<Medicament>();
	private ArrayList <Achat> listeAchat = new ArrayList<Achat>();

	
	public Liste()
	{

	}
	
	
	
	
	public ArrayList<Client> getListeClient() {
		return listeClient;
	}

	public void setListeClient(ArrayList<Client> listeClient) {
		this.listeClient = listeClient;
	}
	
	public void ajouterClient(Client pClient) {
		this.getListeClient().add(pClient) ;
	}
	
	public ArrayList<Medecin> getListeMedecin() {
		return listeMedecin;
	}

	public void setListeMedecin(ArrayList<Medecin> listeMedecin) {
		this.listeMedecin = listeMedecin;
	}
	
	public void ajouterMedecin(Medecin pMedecin) {
		this.getListeMedecin().add(pMedecin) ;
	}
	
	
	public ArrayList<Specialiste> getListeSpecialiste() {
		return listeSpecialiste;
	}

	public void setListeSpecialiste(ArrayList<Specialiste> listeSpecialiste) {
		this.listeSpecialiste = listeSpecialiste;
	}
	
	public void ajouterSpecialiste(Specialiste pSpecialiste) {
		this.getListeSpecialiste().add(pSpecialiste) ;
	}

	public ArrayList<Medicament> getListeMedicament() {
		return listeMedicament;
	}

	public void setListeMedicament(ArrayList<Medicament> listeMedicament) {
		this.listeMedicament = listeMedicament;
	}
	
	public void ajouterMedicament(Medicament pMedicament) {
		this.getListeMedicament().add(pMedicament) ;
	}
	
	public ArrayList<Achat> getListeAchat() {
		return listeAchat;
	}

	public void setListeAchat(ArrayList<Achat> listeAchat) {
		this.listeAchat = listeAchat;
	}

	public void ajoutereAchat(Achat pAchat) {
		this.getListeAchat().add(pAchat) ;
	}
	
}
