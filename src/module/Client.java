package module;

import controleur.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.JList;


public class Client extends Personne {
	
	private String numSecu;
	private LocalDate dateNaissance;
	private String mutuelle;
	
	
	public Client(String nom, String prenom, String adresse, String codePostal, String ville, String telephone, 
				String email, String numSecu, LocalDate dateNaissance, String mutuelle) throws MissMatchException {
		super(nom,prenom,adresse,codePostal,ville,telephone,email);
		this.setNumSecu(numSecu);
		this.setDateNaissance(dateNaissance);
		this.setMutuelle(mutuelle);
	}

	


	@Override
	public String toString() {
		return this.getNom() +" "+ getPrenom();
	}




	public String getNumSecu() {
		return numSecu;
	}



	public void setNumSecu(String numSecu) throws MissMatchException{
		if (numSecu==null||numSecu.trim().isEmpty()) {
			throw new MissMatchException("Saisie du numéro de sécurité sociale incorrecte");}
		if(!Pattern.matches("[12][0-9]{2}(0[1-9]|1[0-2])(2[AB]|[0-9]{2})[0-9]{3}[0-9]{3}([0-9]{2})", numSecu)) {
			throw new MissMatchException("Veuillez entrer un numéro de sécurité sociale valide ");
		}
		else {
		this.numSecu = numSecu;}
	}


	public LocalDate getDateNaissance() {
		return dateNaissance;
	}



	public void setDateNaissance(LocalDate dateNaissance) throws MissMatchException{
		if (dateNaissance==null||dateNaissance.toString().trim().isEmpty()) {
			throw new MissMatchException("Saisie de la date de naissance incorrecte");}
		if(!Pattern.matches("^\\d{4}-\\d{2}-\\d{2}", dateNaissance.toString())) {
			throw new MissMatchException("Veuillez entrer un format de date valide : année/mois/jour ");
		}
		else {
		this.dateNaissance = dateNaissance;}
	}




	public String getMutuelle() {
		return mutuelle;
	}



	public void setMutuelle(String mutuelle) throws MissMatchException{
		if (mutuelle==null||mutuelle.trim().isEmpty()) {
			throw new MissMatchException("Saisie de la mutuelle incorrecte");}
//		if(!Pattern.matches("", mutuelle)) {
//			throw new MissMatchException("Veuillez entrer une mutuelle valide");
//		}
		else {
		this.mutuelle = mutuelle;}
	}
}


	
	
	
	

	
		
		
	
	
	
	
	

	
	
	
	
	