package controleur;

import vue.*;
import module.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
public class Main {
	
	
	

	public static void main(String[] args) throws MissMatchException {
		// TODO Auto-generated method stub
		
		
		
		LocalDate dateNaissanceJean = LocalDate.of(2005, 6, 8);
		LocalDate dateNaissanceMax = LocalDate.of(2005, 6, 8);
		LocalDate dateNaissanceSophie = LocalDate.of(2005, 6, 8);
		LocalDate dateNaissanceIsabelle = LocalDate.of(2005, 6, 8);
		
		
		Client Jean = new Client("Dupont","Jean","8 rue du près", "54000", "Nancy","0601020304","JeanDupont@gmail.com","111111111111111",dateNaissanceJean,"466");
		Client Max = new Client("Dubois","Max","8 rue du près", "54630","Nancy","0601020304","JeanDupont@gmail.com","111111111111111",dateNaissanceMax,"466");
		Client Sophie = new Client("Martin","Sophie","8 rue du près","55810","Nancy","0601020304","JeanDupont@gmail.com","111111111111111",dateNaissanceSophie,"466");
		Client Isabelle = new Client("Petit","Isabelle","8 rue du près","54587","Nancy","0601020304","JeanDupont@gmail.com","111111111111111",dateNaissanceIsabelle,"466");
		
		Liste listes = new Liste();
		listes.ajouterClient(Jean);
		listes.ajouterClient(Max);
		listes.ajouterClient(Sophie);
		listes.ajouterClient(Isabelle);
		
		
		Medecin Luc = new Medecin("Durant","Luc","5 rue saligo","65320","quichonpré","0653965545","LucBlup@gmail.com",58810);
		Medecin Jacques = new Medecin("Marchand","Jacques","8 rue du près","88100","Saint-dié","0624735772","Aroussel@hotmail.fr",755);
		Medecin Claire = new Medecin("Muller","Claire","8 rue du près","88100","Saint-dié","0624735772","Aroussel@hotmail.fr",755);
		
		listes.ajouterMedecin(Luc);
		listes.ajouterMedecin(Jacques);
		listes.ajouterMedecin(Claire);
		
		
		Specialiste Annick = new Specialiste("Roussel","Annick","8 rue du près","88100","Saint-dié","0624735772","Aroussel@hotmail.fr",755,"cardiologue");
		listes.ajouterSpecialiste(Annick);
		
		
		Medicament medicPara = new Medicament("Paracétamol", "Antalgique", 2.18,dateNaissanceJean,500 );
        Medicament medicDafa = new Medicament("Dafalgan", "Antalgique", 4.50,dateNaissanceJean,650 );
        Medicament medicIbu = new Medicament("Ibuprofène", "Antalgique", 3.20,dateNaissanceJean,750 );
        listes.ajouterMedicament(medicPara);
        listes.ajouterMedicament(medicDafa);
        listes.ajouterMedicament(medicIbu);
        
		
		
		HistoriqueAchats historiqueAchat = new HistoriqueAchats();
		Principale frame = new Principale(listes,historiqueAchat);
		frame.setVisible(true);
	}

}
