package vue;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import vue.*;
import controleur.*;
import module.HistoriqueAchats;
import module.Liste;

public class Principale extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Principale frame = new Principale();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public Principale(Liste pliste, HistoriqueAchats pHistoriqueAchats) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		contentPane.setLayout(null);
		
		AchatDirect achatDirect = new AchatDirect(pliste,pHistoriqueAchats);
		achatDirect.setVisible(false);
		
		
		JPanel panLogo = new JPanel();
		panLogo.setBackground(new Color(255, 255, 255));
		panLogo.setBounds(0, 0, 684, 141);
		getContentPane().add(panLogo);
		panLogo.setLayout(null);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(Principale.class.getResource("/media/Logo-Pharmacie.jpg")));
		lblLogo.setBounds(534, 11, 108, 108);
		panLogo.add(lblLogo);
		
		JLabel lblTitre = new JLabel("Pharmacie Sparadrap",SwingConstants.CENTER);
		lblTitre.setBounds(-74, 41, 684, 48);
		panLogo.add(lblTitre);
		lblTitre.setFont(new Font("Serif", Font.BOLD, 40));
		
		JLabel lblTitreAchat = new JLabel("Choisir le type d'achat");
		lblTitreAchat.setBounds(254, 140, 201, 32);
		panLogo.add(lblTitreAchat);
		lblTitreAchat.setFont(new Font("Serif", Font.BOLD, 20));
		
		JPanel panAcceuil = new JPanel();
		panAcceuil.setBounds(0, 129, 684, 521);
		getContentPane().add(panAcceuil);
		panAcceuil.setLayout(null);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(585, 487, 89, 23);
		panAcceuil.add(btnQuitter);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.setBounds(464, 487, 100, 23);
		getContentPane().add(btnRetour);
		btnRetour.setVisible(false);
		
		JButton btnAchats = new JButton("Achats");
		btnAchats.setBounds(300, 72, 89, 23);
		panAcceuil.add(btnAchats);
		
		JButton btnHistAchats = new JButton("Historique des achats");
		btnHistAchats.setBounds(266, 173, 182, 23);
		panAcceuil.add(btnHistAchats);
		
		JButton btnHistOrd = new JButton("Historique des ordonnances");
		btnHistOrd.setBounds(263, 265, 185, 23);
		panAcceuil.add(btnHistOrd);
		
		JButton btnNewButton_4 = new JButton("New button");
		btnNewButton_4.setBounds(300, 365, 89, 23);
		panAcceuil.add(btnNewButton_4);
		
		JButton btnAchatDirect = new JButton("Achat direct");
		btnAchatDirect.setBounds(276, 135, 146, 23);
		panAcceuil.add(btnAchatDirect);
		btnAchatDirect.setVisible(false);
		
		JButton btnAchatOrd = new JButton("Achat via ordonnance");
		btnAchatOrd.setBounds(276, 232, 146, 23);
		panAcceuil.add(btnAchatOrd);
		btnAchatOrd.setVisible(false);
		
		JButton btnLeMdecinTraitant = new JButton("Le médecin traitant");
		btnLeMdecinTraitant.setBounds(80, 193, 171, 23);
		panAcceuil.add(btnLeMdecinTraitant);
		btnLeMdecinTraitant.setVisible(false);
		
		JButton btnUnSpecialiste = new JButton("Un spécialiste");
		btnUnSpecialiste.setBounds(423, 193, 171, 23);
		panAcceuil.add(btnUnSpecialiste);
		btnUnSpecialiste.setVisible(false);
		
		JLabel lblDelivrance = new JLabel("Délivrée par :");
		lblDelivrance.setFont(new Font("Serif", Font.PLAIN, 15));
		lblDelivrance.setBounds(293, 92, 129, 32);
		panAcceuil.add(lblDelivrance);
		lblDelivrance.setVisible(false);
		
		
		
		
		btnAchats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAchats.setVisible(false);
				btnHistAchats.setVisible(false);
				btnHistOrd.setVisible(false);
				btnNewButton_4.setVisible(false);
				btnAchatDirect.setVisible(true);
				btnAchatOrd.setVisible(true);
			}
		});
		
		btnAchatDirect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panAcceuil.setVisible(false);
				getContentPane().add(achatDirect);
				achatDirect.setVisible(true);
				achatDirect.add(btnQuitter);
				achatDirect.add(btnRetour);
				btnRetour.setVisible(true);
				
			}
		});
		
		btnAchatOrd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAchatDirect.setVisible(false);
				btnAchatOrd.setVisible(false);
				lblDelivrance.setVisible(true);
				btnLeMdecinTraitant.setVisible(true);
				btnUnSpecialiste.setVisible(true);
				
				
			}
		});
		
		
		btnHistAchats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panAcceuil.setVisible(false);
				HistoriqueDesAchats historique = new HistoriqueDesAchats(pHistoriqueAchats);
				getContentPane().add(historique);
				historique.setVisible(true);
				historique.add(btnQuitter);
				historique.add(btnRetour);
//				btnRetour.setVisible(true);
			}
		});
		
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				achatDirect.setVisible(false);
//				historique.setVisible(false);
				btnAchats.setVisible(true);
				btnHistAchats.setVisible(true);
				btnHistOrd.setVisible(true);
				btnNewButton_4.setVisible(true);
				btnAchatDirect.setVisible(false);
				btnAchatOrd.setVisible(false);
				panAcceuil.setVisible(true);
			}
		});
		
		
	}
}
